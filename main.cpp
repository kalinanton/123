// ConsoleApplication5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>

namespace Magic {

	template<class H, class U> struct TypeList{
		typedef H Head;
		typedef U Tail;
	};

	struct NullType {};

	template<class TList> struct Length;

	template<> struct Length<NullType> {
		enum { value = 0 };
	};

	template<class TList> struct Length {
		typedef typename TList::Head Head;
		typedef typename TList::Tail Tail;

		enum { value = 1 + Length<Tail>::value };
	};

	template<class TList, int index> struct TypeAt {
		typedef typename TList::Head Head;
		typedef typename TList::Tail Tail;

		template<int i> struct In {
			typedef typename TypeAt<Tail, i - 1>::Result Result;
		};

		template<> struct In<0> {
			typedef Head Result;
		};

		typedef typename In<index>::Result Result;
	};

	template<class TList, int i> struct Data_Int;

	template<class TList> struct Data_Int<TList,0> {
		NullType data;
	};

	template<class TList, int i> struct Data_Int {
		typedef typename TList::Tail Tail;
		typedef typename TList::Head Head;

		typename Head data;
		Data_Int<Tail, i - 1> Data_List;

		Data_Int() : data(), Data_List() {};

		template<int j> struct Const_Taker {
			const typename TypeAt<TList, j>::Result * ResultPtr;
			Const_Taker(const Data_Int<TList, i> * DATA_INT) : ResultPtr(Data_Int<Tail, i - 1>::Const_Taker<j - 1>(&DATA_INT->Data_List).ResultPtr) {}
		};

		template<> struct Const_Taker<0> {
			const Head * ResultPtr;
			Const_Taker(const Data_Int<TList, i> * DATA_INT) : ResultPtr(&DATA_INT->data) {}
		};

		template<int j> struct Taker {
			typename TypeAt<TList, j>::Result * ResultPtr;
			Taker(Data_Int<TList, i> * DATA_INT) : ResultPtr(Data_Int<Tail, i - 1>::Taker<j - 1>(&DATA_INT->Data_List).ResultPtr) {}
		};

		template<> struct Taker<0> {
			Head * ResultPtr;
			Taker(Data_Int<TList, i> * DATA_INT) : ResultPtr(&DATA_INT->data) {}
		};

	};

	template<class TList> struct Data {	
		Data_Int<TList, Length<TList>::value> DATA_LIST;

		template<int i> const typename TypeAt<TList, i>::Result * Get() const { // get const pointer to i's data member
			Data_Int<TList, Length<TList>::value>::Const_Taker<i> Const_Taker(&DATA_LIST);
			return Const_Taker.ResultPtr;
		}

		template<int i> typename TypeAt<TList, i>::Result * Get() { // get pointer to i's data member
			Data_Int<TList, Length<TList>::value>::Taker<i> Taker(&DATA_LIST);
			return Taker.ResultPtr;
		}

	};
}

class Student {
private:
	typedef Magic::TypeList<unsigned int, 
		Magic::TypeList<char*, 
		Magic::TypeList<std::vector<unsigned int>, 
		Magic::TypeList<double, Magic::TypeList<bool, 
		Magic::NullType>>>>> Stud_Data_Type;

	Magic::Data<Stud_Data_Type> DATA;

public:

	enum {
		age,
		name,
		marks,
		height,
		gender
	};

	Student() : DATA() {}
	~Student() {}

	template<int i> typename Magic::TypeAt<Stud_Data_Type, i>::Result & Get() { //non-const getter
		return *DATA.Get<i>();
	}

	template<int i> const typename Magic::TypeAt<Stud_Data_Type, i>::Result & Get() const { //const getter
		return *DATA.Get<i>();
	}
};


int main()
{

	int a;
	std::cin >> a;

    return 0;
}